<?php
Plugininformationen /* 
Plugin Name: Testplugin - Basiert auf dem Link-Manager Plugin von Wordpress.com
Plugin URI: http://wobintosh.de/
Description: Test, test, eins, zwo, drei!
Version: 1.0
Author: Fabian Gründling
Author URI: http://wobintosh.de.de/
Update Server: http://wobintosh.de/wp-content/download/wp/
Min WP Version: 3.4
Max WP Version: 3.5
*/
GNU-Lizenz /*  Copyright 2013  Fabian Gründling  (fabian@wobintosh.de)
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*function Blogroll () {
  echo "Blogroll"; 
	
	}						*/

add_filter( 'pre_option_link_manager_enabled', '__return_true' );

?>
